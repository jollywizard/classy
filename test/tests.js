const src = '../src'

const assert = require('assert')

// Mockup paremeter data.
class A{}
class B{}
class C{}
class D{}

/**
  Creates a mockup module object with the required fields.
*/
function mock_module()
{
  return {exports:{}}
}

/**
  This function verifies that the classes are installed at the expected location.
*/
function check_class(module, ...classes)
{
  for (const _class of classes)
  {
    const name = _class.name
    assert(!!module.exports.class[name])
  }
}

describe('`index` module:', function() {
  it('can be imported:', ()=>require('..') )
})

describe('`index` module Function:', function() {

  const Classy = require('..')

  it ('accepts a module as a parameter.', function() {
    const module = mock_module()
    Classy(module)
  })

  it ('will add all subsequent parameters to `module.exports.class`.', function() {
    const module = mock_module()
    Classy(module,A,B)
    check_class(module, A, B)
  })

  it ('returns a function which can be called to register more parameters.', function() {
    const module = mock_module()
    Classy(module)(A,B)
    check_class(module, A, B)
  })

  it ('can be used in both modes together.', function() {
    const module = mock_module()

    Classy(module,A,B)(C,D)
    check_class(module, A, B, C, D)
  })
})
