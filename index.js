
/**
  Creates a helper which adds classes to a modules exports.
*/
function Classy(module,...classes)
{
  if (!module)
    throw new TypeError('module` is not valid.')

  if (!module.exports)
    throw new TypeError('`module.exports` is not defined.')

  const curry = ClassyCurry(module.exports)
  curry(...classes)
  return curry
}

/**
  Default implementation for `ClassCurry(:keymap)`.

  @return The name property of the value.
  @throws {TypeError} If the name property does not exist.
*/
function Namer (value) {
    if(!value.name)
      throw new TypeError(`Value must have a name field. ${value}`)

    return value.name
}

/**
  Creates a curry function which feeds the classes into the nested class parameter.
*/
function ClassyCurry(target, key = 'class', keymap = Namer)
{
  const curry = function(...members)
  {
    for (const member of members)
    {
      let name = keymap(member)

      if (!target[key])
        target[key] = {}

      target[key][name] = member
    }
  }

  return curry
}

Classy.curry = ClassyCurry

module.exports = Classy
